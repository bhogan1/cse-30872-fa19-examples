#!/bin/sh

# Check that user has specified a lockbox
if [ $# -ne 1 ]; then
    echo "Usage: crack.sh executable"
    exit 1
fi
LOCKBOX=$(readlink -f $1)

# Check that lockbox is readable
if [ ! -r $LOCKBOX ]; then
    echo "$LOCKBOX is not readable!"
    exit 2
fi

# Check that lockbox is executable
if [ ! -x $LOCKBOX ]; then
    echo "$LOCKBOX is not executable!"
    exit 3
fi

# For each string in lockbox, attempt to unlock it with the string as a
# password until a valid token is found
for password in $(strings $LOCKBOX); do
    token=$($LOCKBOX $password);
    if [ -n "$token" ]; then
    	echo "Password is $password"
    	echo "Token    is $token"
    	exit 0
    fi
done

# We were not able to crack the password, so exit with failure
echo "Unable to crack $LOCKBOX"
exit 4
