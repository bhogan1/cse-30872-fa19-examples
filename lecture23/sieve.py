#!/usr/bin/env python3

# Functions

def compute_primes(n):
    ''' Use Sieve of Eratosthenes to compute primes from 2 through n. '''
    primes = set(range(2, n + 1))

    for i in range(2, n + 1):
        for m in range(i*2, n + 1, i):
            try:
                primes.remove(m)
            except KeyError:
                pass

    return primes

# Main execution

if __name__ == '__main__':
    primes = compute_primes(100)
    print(len(primes))
    print(primes)
