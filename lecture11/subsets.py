#!/usr/bin/env python3

import itertools

# Constants

NUMBERS = range(0, 10)
COUNT   = 0

# Main Execution

if __name__ == '__main__':
    for length in range(0, len(NUMBERS) + 1):
        for subset in itertools.combinations(NUMBERS, length):
            if sum(subset) % 3 == 0:
                COUNT += 1

    print(COUNT)
