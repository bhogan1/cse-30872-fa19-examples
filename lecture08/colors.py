#!/usr/bin/env python3

import sys

# Main execution

if __name__ == '__main__':
    for line in sys.stdin:
        counts = [0, 0, 0]

        for i in map(int, line.split()):
            counts[i] += 1

        result = []
        for i, v in enumerate(counts):
            result.extend([i]*v)

        print(' '.join(map(str, result)))
